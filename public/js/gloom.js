var ROAD_TYPE = 0;
var CITY_TYPE = 1;
var app = new Vue({ 
	el: '#app',
	data: {
		events: [[{"text": "none"}], [{"text": "none"}]],
		eventType: 0, // 0 pour road, 1 pour city
		eventId: 1,
    option: "",
		message: "cc",
	},
	computed: {
		mevents: function() {
			return this.events[this.eventType];
		},
		mevent: function() {
			let res = this.mevents[this.eventId];
			return res;
		}
	},
	// the app methods
	methods: {
		selectRoad: function() {
			this.eventType = ROAD_TYPE;
      this.option = "";
		},
		selectCity: function() {
			this.eventType = CITY_TYPE;
      this.option = "";
		},
    setOption: function (option){
      console.log("set option to " + option);
      this.option = option;
    }
	},
	// autorun at load
	mounted: function (){
		var self = this;
		$.getJSON("./js/road.json", json => {
            console.log("loaded road events", json);
			Vue.set(self.events, ROAD_TYPE, json);
		});
		$.getJSON("./js/city.json", json => {
            console.log("loaded city events", json);
			Vue.set(self.events, CITY_TYPE, json);
		});
	},
});
app.eventId = 1;
