#!/usr/bin/env python
# encoding: utf-8

import pytest

from gloom_scenario import Party
from gloom_scenario import Scenario
from gloom_scenario import parse


def get_scen(id, achievements, locations, req_completes, req_incompletes):
    return Scenario(
        {
            "id": id,
            "achievements": achievements,
            "locations": locations,
            "req_completes": req_completes,
            "req_incompletes": req_incompletes,
        }
    )


def test_scenario():
    sc0 = get_scen(0, ["DONE_0"], [1], [], [])
    sc1 = get_scen(1, ["DONE_1"], [2, 3, 4], [], [])
    party = Party({}, done_file=None)
    party.add_scenario(sc0)
    party.add_scenario(sc1)
    assert party.achievements == set()
    assert party.visited_locations == set()
    assert party.open_locations == set()

    party.do(sc0.id)
    assert party.achievements == set(("DONE_0",))
    assert party.visited_locations == set((0,))
    assert party.open_locations == set((1,))
    party.do(sc1.id)
    assert party.achievements == set(("DONE_0", "DONE_1"))
    assert party.achievements == set(("DONE_0", "DONE_1"))
    assert party.open_locations == set((2, 3, 4))
    ## can visit?
    sc2 = get_scen(2, ["DONE_2"], [3], ["DONE_0"], [])
    party.add_scenario(sc2)
    assert party.can_visit(2) is True
    assert party.open_locations == set((2, 3, 4))

    sc3 = get_scen(3, ["DONE_3"], [], ["DONE_2"], [])
    party.add_scenario(sc3)
    assert party.can_visit(3) is False
    assert party.open_locations == set((2, 3, 4))

    sc4 = get_scen(4, ["DONE_4"], [], ["DONE_0", "DONE_1"], [])
    party.add_scenario(sc4)
    assert party.can_visit(4) is True
    assert party.open_locations == set((2, 3, 4))

def test_real_scenarios():
    party = Party(done_file=None)
    assert party.can_visit(7) == False
    # si on reussit le 11, on ne peut plus faire le 12
    party.do(12)
    assert party.can_visit(11) == False
    # si on reussit le 32, on peut faire le 33
    assert party.can_visit(33) == False
