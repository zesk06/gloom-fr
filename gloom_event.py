#!/usr/bin/env python
# encoding: utf-8

import re
import json
import click


class Carte:
    def __init__(self):
        self.id = -1
        self.text = None
        self.option_a = None
        self.option_b = None
        self.resolution_a = None
        self.resolution_b = None

    def check(self, id=None):
        if id is not None and self.id != id:
            print("CHECK error: Pb sur les IDs de cartes")
            __import__('ipdb').set_trace()
            assert id == self.id, f"assert error on {self.json}"
        if self.text is None:
            click.echo(f"{self.id} text is none")
        if self.option_a is None:
            click.echo(f"{self.id} option_a is none")
        if self.option_b is None:
            click.echo(f"{self.id} option_b is none")
        if self.resolution_a is None:
            click.echo(f"{self.id} resolution_a is none")
        if self.resolution_b is None:
            click.echo(f"{self.id} resolution_b is none")

    @property
    def json(self):
        return {
            "id": self.id,
            "text": self.text,
            "option_a": self.option_a,
            "option_b": self.option_b,
            "resolution_a": self.resolution_a,
            "resolution_b": self.resolution_b,
        }


IN_NON = -1
IN_TXT = 0
IN_OPA = 1
IN_OPB = 2
IN_REA = 3
IN_REB = 4


def parse(data_file="datas/cartes_ville.txt"):
    cards = []
    card = Carte()
    state = IN_NON
    with open(data_file, "r") as inputf:
        for line in inputf:
            line = line.strip()
            if "--------------------------" in line:
                card.check(id=len(cards)+1)
                cards.append(card)
                # click.echo(f"added card {card.id}")
                card = Carte()
                state = IN_NON
                continue
            card_ids = re.findall(r"^Carte (\d+) :", line)
            if card_ids:
                card.id = int(card_ids[0])
                state = IN_TXT
                card.text = ""
                line = ""
            elif "Option A :" in line:
                card.option_a = ""
                line = line.replace("Option A :", "")
                state = IN_OPA
            elif "Option B :" in line:
                card.option_b = ""
                line = line.replace("Option B :", "")
                state = IN_OPB
            elif "Résolution A :" in line:
                card.resolution_a = ""
                line = line.replace("Résolution A :", "")
                state = IN_REA
            elif "Résolution B :" in line:
                card.resolution_b = ""
                line = line.replace("Résolution B :", "")
                state = IN_REB
            if state == IN_TXT:
                card.text += line
            elif state == IN_OPA:
                card.option_a += line
            elif state == IN_OPB:
                card.option_b += line
            elif state == IN_REA:
                card.resolution_a += line
            elif state == IN_REB:
                card.resolution_b += line
    # click.echo(f"found {len(cards)} cards")
    return cards


def to_json(cards, output_f="datas/cartes_ville.json"):
    with open(output_f, "w") as output_s:
        json.dump([card.json for card in cards], output_s, indent=True)
    click.secho(f"wrote {output_f}", fg="green")


@click.command()
@click.argument("id", type=click.INT)
@click.option(
    "--city",
    is_flag=True,
    help="si indiqué, on fait un event de ville au lieu de route",
)
def main(id, city):
    if city:
        click.secho(f"=== City Event {id} ===", fg="yellow")
        cards = parse("datas/cartes_ville.txt")
    else:
        click.secho(f"=== Road Event {id} ===", fg="yellow")
        cards = parse("datas/cartes_route.txt")
    card = cards[int(id)-1]
    click.secho(card.text)
    click.secho("")
    click.secho(f"Option A: {card.option_a}\n", fg="bright_green")
    click.secho(f"Option B: {card.option_b}\n", fg="bright_cyan")
    click.secho(f">> your choice? a/b \n", fg="magenta")
    choice = click.getchar()
    if choice.lower() == "a":
        click.secho(f"Resolution A: {card.resolution_a}", fg="yellow")
    elif choice.lower() == "b":
        click.secho(f"Resolution B: {card.resolution_b}", fg="yellow")
    else:
        click.secho("Déso dude, faut entrer la touche 'a' ou la touche 'b'", fg="red")


if __name__ == "__main__":
    main()
