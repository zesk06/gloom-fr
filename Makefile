all: scenarios.png

scenarios.png: scenarios.dot
	@dot -T png -o scenarios.png scenarios.dot

scenarios.dot: datas/scenarios.yml datas/scenarios_done.yml
	@python gloom_scenario.py -o scenarios.dot

clean:
	@rm -f scenarios.dot scenarios.png
