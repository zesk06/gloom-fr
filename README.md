# gloom-fr

des scripts pour jouer à gloomhaven en Français.
Les traductions des cartes/scénarios ne sont pas de moi, mais proviennent
de [ce site](https://drive.google.com/drive/folders/0B_7GtAv5lDrhcG9xaU1rRmhQZkk)

## install

```bash
# necessite python3 et pip3
pip install -r requirements
```

## gloom_event

Permet de tirer des évènements de route/ville et de choisir une option A/B.
Evite de voir la réponse.

```bash
# tirer l'évènement de route numéro 10
python gloom_event.py 10

# tirer l'évènement de ville numéro 24
python gloom_event.py --city 24
```

[![asciicast](https://asciinema.org/a/l2V2uFZwrIdCJB01aja4dA8JJ.svg)](https://asciinema.org/a/l2V2uFZwrIdCJB01aja4dA8JJ)

## gloom_scenario

Permet de savoir les quêtes accessibles en fonction des quêtes réalisées.
Il faut rentrer les quêtes faites dans le fichier ``datas/scenarios_done.yml`` et lancer 
le script.

[![asciicast](https://asciinema.org/a/283829.svg)](https://asciinema.org/a/283829)

