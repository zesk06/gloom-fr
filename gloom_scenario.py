#!/usr/bin/env python
# encoding: utf-8

import re
import json
import click
import yaml
from jinja2 import Template
from path import Path
from dotted.collection import DottedDict
from logzero import logger

class Scenario(DottedDict):
    def __init__(self, json_obj):
        super(Scenario, self).__init__(json_obj)
        for prop in (
            "achievements",
            "achievements_lost",
            "req_completes",
            "req_completes_oneof",
            "req_incompletes",
            "locations",
        ):
            if prop not in self:
                self[prop] = set()
        if "visited" not in self:
            self.visited = False


def parse(data_file="datas/scenarios.yml"):
    scen_path = Path("datas/scenarios.yml")
    scenarios = yaml.safe_load(open(scen_path))
    return {scenario_json["id"]: Scenario(scenario_json) for scenario_json in scenarios}


class Party:
    def __init__(self, scenarios=None, done_file="datas/scenarios_done.yml"):
        self.scenarios = scenarios
        if self.scenarios is None:
            self.scenarios = parse()
        self.scenarios_done = set()
        self.__achievements = set()
        self.__achievements_lost = set()
        self.done_file = done_file

    def add_scenario(self, scenario):
        self.scenarios[scenario.id] = scenario

    def do(self, scenario_id):
        self.scenarios_done.add(scenario_id)
        scenario = self.scenarios[scenario_id]
        scenario.visited = True
        self.__achievements.update(scenario.achievements)
        self.__achievements_lost.update(scenario.achievements_lost)

    @property
    def done_infos(self):
        if self.done_file:
            return yaml.safe_load(open(self.done_file, "r"))
        return {"achievements": [], "locations": []}

    @property
    def achievements(self):
        extra_achievements = self.done_infos["achievements"]
        return self.__achievements.difference(self.__achievements_lost).union(
            extra_achievements
        )

    @property
    def open_locations(self):
        """renvoie les lieux ouverts, mais peut être non visitables"""
        open_locations = set(
            [
                location
                for scenario in self.scenarios.values()
                for location in scenario.locations
                if scenario.visited
            ]
        )
        # add extra open locations
        open_locations.update(self.done_infos["locations"])
        # remove visited ones
        return open_locations.difference(self.visited_locations)

    @property
    def visited_locations(self):
        return set(
            [scenario.id for scenario in self.scenarios.values() if scenario.visited]
        )

    def can_visit(self, scenario_id):
        visitable = True
        scenario = self.scenarios[scenario_id]
        if scenario.req_completes:
            # on doit matcher TOUS les scenarios
            matching_req = self.achievements.intersection(scenario.req_completes)
            visitable = len(matching_req) == len(scenario.req_completes)
        elif scenario.req_completes_oneof:
            # on doit matcher au moins UN
            matching_req = self.achievements.intersection(scenario.req_completes_oneof)
            visitable = len(matching_req) >= 1
        # on ne doit avoir aucun (0) matching req avec les incompletes
        if scenario.req_incompletes:
            matching_req = self.achievements.intersection(scenario.req_incompletes)
            visitable = visitable and len(matching_req) == 0
        return visitable


def generate_dot(party, all):
    """genere le .dot
    :param scenarios: La liste de scenarios
    :param all: si True, affiche tous les scenarios, sinon n'affiche que les visités
    """
    template = Template(Path("datas/scenario_dot.jinja").text())
    # put visited scenarios last in the array
    return template.render(party=party, all_scenarios=all)


@click.command()
@click.option("--output-file", "-o", help="le fichier de sortie DOT")
@click.option("--all/--visited", "-a", help="Affiches tous les noeuds")
def main(all, output_file):
    scenarios = parse()

    party = Party(scenarios)
    for id in yaml.safe_load(open("datas/scenarios_done.yml"))["done"]:
        id = int(id)
        party.do(id)

    dot_content = generate_dot(party, all)
    if output_file:
        Path(output_file).write_text(dot_content)
        click.secho(f"generated dot file {output_file}", fg="red")
    click.secho("== Party Achievements ==", fg="yellow")
    for achievement in sorted(party.achievements):
        click.secho(f"- {achievement}")

    click.secho("== Open locations ==", fg="yellow")
    for scenario in sorted(party.open_locations):
        msg = f"- {scenario}: {scenarios[scenario].name}"
        color = "green"
        if not party.can_visit(scenario):
            req = party.scenarios[scenario].req_completes
            unreq = party.scenarios[scenario].req_incompletes
            msg += f"  [REQ:{req} UNREQ:{unreq}]"
            color = "red"
        click.secho(msg, fg=color)


if __name__ == "__main__":
    main()
